#include "house.h"
static std::vector<GLfloat> _HOUSE_VERTICES =
{
        0.0f, -0.0f, 0.0f,  0.0f, 0.0f,       //cube_backside
        1.0f, -0.0f, 0.0f,  1.0f, 0.0f,
        1.0f,  1.0f, 0.0f,  1.0f, 1.0f,
        1.0f,  1.0f, 0.0f,  1.0f, 1.0f,
        0.0f,  1.0f, 0.0f,  0.0f, 1.0f,
        0.0f, -0.0f, 0.0f,  0.0f, 0.0f,

        0.0f, -0.0f,  1.0f,  0.0f, 0.0f,       //cube_frontside
        1.0f, -0.0f,  1.0f,  1.0f, 0.0f,
        1.0f,  1.0f,  1.0f,  1.0f, 1.0f,
        1.0f,  1.0f,  1.0f,  1.0f, 1.0f,
        0.0f,  1.0f,  1.0f,  0.0f, 1.0f,
        0.0f, -0.0f,  1.0f,  0.0f, 0.0f,

        0.0f,  1.0f,  1.0f,  1.0f, 1.0f,       //cube_leftside
        0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
        0.0f, -0.0f,  0.0f,  0.0f, 0.0f,
        0.0f, -0.0f,  0.0f,  0.0f, 0.0f,
        0.0f, -0.0f,  1.0f,  1.0f, 0.0f,
        0.0f,  1.0f,  1.0f,  1.0f, 1.0f,

        1.0f,  1.0f,  1.0f,  1.0f, 1.0f,       //cube_rightside
        1.0f,  1.0f,  0.0f,  0.0f, 1.0f,
        1.0f, -0.0f,  0.0f,  0.0f, 0.0f,
        1.0f, -0.0f,  0.0f,  0.0f, 0.0f,
        1.0f, -0.0f,  1.0f,  1.0f, 0.0f,
        1.0f,  1.0f,  1.0f,  1.0f, 1.0f,

        0.0f, -0.0f,  0.0f,  0.0f, 1.0f,       //cube_bottomside
        1.0f, -0.0f,  0.0f,  1.0f, 1.0f,
        1.0f, -0.0f,  1.0f,  1.0f, 0.0f,
        1.0f, -0.0f,  1.0f,  1.0f, 0.0f,
        0.0f, -0.0f,  1.0f,  0.0f, 0.0f,
        0.0f, -0.0f,  0.0f,  0.0f, 1.0f,

        0.0f,  1.0f, 0.0f,  0.0f, 1.0f,       //cube_topside
        1.0f,  1.0f, 0.0f,  1.0f, 1.0f,
        1.0f,  1.0f, 1.0f,  1.0f, 0.0f,
        1.0f,  1.0f, 1.0f,  1.0f, 0.0f,
        0.0f,  1.0f, 1.0f,  0.0f, 0.0f,
        0.0f,  1.0f, 0.0f,  0.0f, 1.0f
};

House::House(glm::vec3& position, GLuint texID, Texture &texobject, bool horizontal)
    : BaseObject(VAO, VBO, texID, scale, position, window)
{
    if(horizontal)
        scale = glm::vec3(WIDTH, HEIGHT, DEPTH);
    else
        scale = glm::vec3(DEPTH, HEIGHT, WIDTH);
    texobj = &texobject;
    NUM_VERT = 64;
    NUM_VERT_COMPONENTS = 3;
    NUM_TEX_COMPONENTS = 2;
    setGeometry(_HOUSE_VERTICES);
    init_vaos();
    windows.reserve(16*3);
    if(horizontal)
        fillwindows_h();
    else
        fillwindows_v();
}

House::House(House &&other)
{
    camera = other.camera;
    position = other.position;
    texID = other.texID;
    VAO = other.VAO;
    VBO = other.VBO;
    window = other.window;
    if(horizontal)
        fillwindows_h();
    else
        fillwindows_v();
}

House::House(House& other)
    : BaseObject(other.VAO, other.VBO, other.texID, other.scale, other.position, other.window)
{
    setGeometry(_HOUSE_VERTICES);
    init_vaos();
    if(horizontal)
        fillwindows_h();
    else
        fillwindows_v();
}

void House::draw(GLFWwindow *window, Camera &camera, Shader &shader)
{
    BaseObject::draw(window, camera, shader);
    for(auto &i: windows)
    {
        i.draw(window, camera, shader);
    }
}

void House::setTexobj(Texture &texobject)
{
    texobj = &texobject;
}

void House::fillwindows_h()
{
    for(float j = 0.3; j < 1.6; j +=0.4)
        for(float i = 0.1; i < 4; i += 0.4)
        {
            windows.emplace_back(position + glm::vec3(i, j, 0), glm::vec3(1, 1, 1), texobj->getTextureWindow());
        }
}

void House::fillwindows_v()
{
    for(float j = 0.3; j < 1.6; j +=0.4)
        for(float i = 0.1; i < 4; i += 0.4)
        {
            windows.emplace_back(position + glm::vec3(-0.1, j, i), glm::vec3(1, 1, 1), texobj->getTextureWindow());
        }
}

void House::drawWindowPosition()
{
    for(auto i: windows)
    {
        i.drawPosition();
    }
}

int House::getNumWindows()
{
    return windows.size();
}

void House::setOrientation(bool horizontal)
{
    this->horizontal = horizontal;
}

std::vector<glm::vec3> House::getWindowCorners()
{
    std::vector<glm::vec3> corners;
    for(auto i: windows)
    {
        for(auto j: i.getCorners())
        {
            corners.push_back(j);
        }
    }
}
