#include "window.h"

static float LENGTH = 0.2;
std::vector<GLfloat> _WINDOW_VERTICES = {
    0.0f, -0.0f, -0.001f,  0.0f, 0.0f,       //cube_backside
    0.2f, -0.0f, -0.001f,  1.0f, 0.0f,
    0.2f,  0.2f, -0.001f,  1.0f, 1.0f,
    0.2f,  0.2f, -0.001f,  1.0f, 1.0f,
    0.0f,  0.2f, -0.001f,  0.0f, 1.0f,
    0.0f, -0.0f, -0.001f,  0.0f, 0.0f,

    0.0f, -0.0f,  0.0f,  0.0f, 0.0f,       //cube_frontside
    0.2f, -0.0f,  0.0f,  1.0f, 0.0f,
    0.2f,  0.2f,  0.0f,  1.0f, 1.0f,
    0.2f,  0.2f,  0.0f,  1.0f, 1.0f,
    0.0f,  0.2f,  0.0f,  0.0f, 1.0f,
    0.0f, -0.0f,  0.0f,  0.0f, 0.0f,

    0.0f,  0.2f,  0.0f,  1.0f, 1.0f,       //cube_leftside
    0.0f,  0.2f, -0.001f,  0.0f, 1.0f,
    0.0f, -0.0f, -0.001f,  0.0f, 0.0f,
    0.0f, -0.0f, -0.001f,  0.0f, 0.0f,
    0.0f, -0.0f,  0.0f,  1.0f, 0.0f,
    0.0f,  0.2f,  0.0f,  1.0f, 1.0f,

     0.2f,  0.2f,  0.0f,  1.0f, 1.0f,       //cube_rightside
     0.2f,  0.2f, -0.001f,  0.0f, 1.0f,
     0.2f, -0.0f, -0.001f,  0.0f, 0.0f,
     0.2f, -0.0f, -0.001f,  0.0f, 0.0f,
     0.2f, -0.0f,  0.0f,  1.0f, 0.0f,
     0.2f,  0.2f,  0.01f,  1.0f, 1.0f,

    0.0f, -0.0f, -0.001f,  0.0f, 1.0f,       //cube_bottomside
     0.2f, -0.0f, -0.001f,  1.0f, 1.0f,
     0.2f, -0.0f,  0.0f,  1.0f, 0.0f,
     0.2f, -0.0f,  0.0f,  1.0f, 0.0f,
    0.0f, -0.0f,  0.0f,  0.0f, 0.0f,
    0.0f, -0.0f, -0.001f,  0.0f, 1.0f,

    0.0f,  0.2f, -0.001f,  0.0f, 1.0f,       //cube_topside
     0.2f,  0.2f, -0.001f,  1.0f, 1.0f,
     0.2f,  0.2f,  0.0f,  1.0f, 0.0f,
     0.2f,  0.2f,  0.0f,  1.0f, 0.0f,
    0.0f,  0.2f,  0.0f,  0.0f, 0.0f,
    0.0f,  0.2f, -0.001f,  0.0f, 1.0f
};

Window::Window(glm::vec3 position, glm::vec3 scale, GLuint texID)
    : BaseObject(VAO, VBO, texID, scale, position, window)
{
    NUM_VERT = 64;
    NUM_VERT_COMPONENTS = 3;
    NUM_TEX_COMPONENTS = 2;
    setGeometry(_WINDOW_VERTICES);
    init_vaos();
}

Window::Window(Window &&other)
{
    camera = other.camera;
    position = other.position;
    texID = other.texID;
    VAO = other.VAO;
    VBO = other.VBO;
    window = other.window;
    std::cout << "Window::MoveConstructor" << std::endl;
    setGeometry(_WINDOW_VERTICES);
    init_vaos();
}

Window::Window(Window& other)
    : BaseObject(other.VAO, other.VBO, other.texID, other.scale, other.position, other.window)
{

}

std::vector<glm::vec3> Window::getCorners()
{
    std::vector<glm::vec3> wincorners;
    wincorners.push_back(position + glm::vec3(LENGTH, LENGTH, 0));    //Top right
    wincorners.push_back(position + glm::vec3(-LENGTH, LENGTH, 0));   //Top left
    wincorners.push_back(position + glm::vec3(-LENGTH, -LENGTH, 0));  //Bottom left
    wincorners.push_back(position + glm::vec3(LENGTH, -LENGTH, 0));   //Bottom right
    return wincorners;
}
