#include "baseobject.h"
#include <glm/gtx/string_cast.hpp>

BaseObject::BaseObject()
{

}

BaseObject::BaseObject(GLuint VAO, GLuint VBO, GLuint texID, glm::vec3& scale, glm::vec3& position, GLFWwindow *window)
    : VAO(VAO), VBO(VBO), texID(texID), scale(scale), position(position), window(window)
{

}


BaseObject::~BaseObject()
{
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
}

glm::vec3 BaseObject::getPosition()
{
    return position;
}

void BaseObject::drawPosition()
{
    std::cout << "(" << position[0] << " " << position[1] << " " << position[2] << ")" << std::endl;
}

void BaseObject::draw(GLFWwindow *window, Camera &camera, Shader &shader)
{
    checkGLErrors("BaseObject::Start of game loop::");
    shader.Use();
    checkGLErrors("BaseObject::Use Shader::");

    // Bind Textures using texture units
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texID);
    glUniform1i(glGetUniformLocation(shader.Program, "Texture"), 0);
    checkGLErrors("BaseObject::Bind texture::");

    // Create camera transformation
    glm::mat4 view;
    view = camera.GetViewMatrix();
    glm::mat4 projection;
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    projection = glm::perspective(camera.Zoom, (float)width/(float)height, 0.1f, 1000.0f);
    // Get the uniform locations
    GLint modelLoc = glGetUniformLocation(shader.Program, "model");
    GLint viewLoc = glGetUniformLocation(shader.Program, "view");
    GLint projLoc = glGetUniformLocation(shader.Program, "projection");
    checkGLErrors("BaseObject::View and projection matrices::");

    // Pass the matrices to the shader
    glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projLoc, 1, GL_FALSE, glm::value_ptr(projection));
    checkGLErrors("BaseObject::Passed matrices to shader::");

    glBindVertexArray(VAO);
    checkGLErrors("BaseObject::Bind VAO::");
    glm::mat4 model;
    //model = glm::scale_slow(model, scale);
    //model = glm::translate(model, position);
    transform(model, position, scale);
    glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));
    checkGLErrors("BaseObject::Apply scale and translation::");

    glDrawArrays(GL_TRIANGLES, 0, 36);
    checkGLErrors("BaseObject::Draw::");

    glBindVertexArray(0);
}

void checkGLErrors(std::__cxx11::string errorstring)
{
    GLenum err = glGetError();
    if(err != GL_NO_ERROR)
        std::cerr << errorstring << gluErrorString(err) << std::endl;
}

void BaseObject::init_vaos()
{
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &VBO);
    glEnableVertexAttribArray(0);
    checkGLErrors("BaseObject::Arrays generated::");
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    checkGLErrors("BaseObject::Buffers bound::");

    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * NUM_VERT * NUM_VERT_COMPONENTS/*this->vertices.size()*/, &this->vertices[0], GL_STATIC_DRAW);

    checkGLErrors("BaseObject::Buffers initilized::");

    // Position attribute
    glVertexAttribPointer(0, NUM_VERT_COMPONENTS, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    checkGLErrors("BaseObject::Position vertices filled::");

    // TexCoord attribute
    glVertexAttribPointer(2, NUM_TEX_COMPONENTS, GL_FLOAT, GL_FALSE, 5 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(2);

    glBindVertexArray(0); // Unbind VAO

    checkGLErrors("BaseObject::Buffer filled::");
}

void BaseObject::setGeometry(std::vector<GLfloat>& vertices)
{
    this->vertices = vertices;
}

void BaseObject::setScale(glm::vec3 newscale)
{
    scale = newscale;
}

std::vector<glm::vec3> BaseObject::getCorners()
{
    std::vector<glm::vec3> corners;
    for(short i = 0; i < 4; i ++)
    {
        corners.push_back(position);
    }
    return corners;
}

glm::mat4x4 transform(glm::mat4  & model, glm::vec3 position, glm::vec3 scale)
{
    /* |s1  x  x  p1|
     * |x   s2 x  p2|
     * |x   x  s3 p3|
     * |x   x  x  1 |*/
    model[0][0] = scale[0];
    model[1][1] = scale[1];
    model[2][2] = scale[2];

    model[3][0] = position[0];
    model[3][1] = position[1];
    model[3][2] = position[2];

    return model;
}

void printvec3(glm::vec3 vector)
{
    std::cout << vector[0] << " " << vector[1] << " " << vector[2] << " ";
}
