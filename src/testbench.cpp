#include "house.h"
#include "street.h"
#include "window.h"
#include "texture.h"
#include "shader.h"

#include <iostream>

GLFWwindow* init();
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void fillObjects(std::vector<House> &houses, std::vector<Street> &streets);

// Camera
Camera camera(glm::vec3(3.0f, 3.0f, 0.0f));
bool keys[1024];
GLfloat lastx = 400, lasty = 300, lastFrame = 0.0f, deltaTime;
bool firstMouse = true;

GLFWwindow* init();


int main()
{
    GLFWwindow* window = init();
    Shader shader("../include/vshader.vs", "../include/fshader.frag");

    std::vector<House> houses;
    std::vector<Street> streets;

    fillObjects(houses, streets);

    while(!glfwWindowShouldClose(window))
    {
        if(window == nullptr)
        {
            std::cerr << "BaseObject::Window points to nothing!" << std::endl;
        }
        // Set frame time
        GLfloat currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // Check and call events
        glfwPollEvents();
        doMovement();

        checkGLErrors("BaseObject::Event stuff::");

        // Clear the colorbuffer
        glClearColor(0.1f, 0.1f, 0.1f, 0.7f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //houses[0].draw(window, camera, shader);
        for(auto &i: houses)
        {
            i.draw(window, camera, shader);
        }

        for(auto &j: streets)
        {
            j.draw(window, camera, shader);
        }

        // Swap the buffers
        glfwSwapBuffers(window);
    }

    checkGLErrors("Main::End of programm: ");

}


GLFWwindow* init()
{
    GLenum err;
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    glfwWindowHint(GLFW_SAMPLES, 4);

    checkGLErrors("Window hints: ");

    const GLFWvidmode* mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
    glfwWindowHint(GLFW_RED_BITS, mode->redBits);
    glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
    glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
    glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);

    checkGLErrors("Moar window hints: ");

    GLFWwindow* window = glfwCreateWindow(mode->width, mode->height, "Testbench", glfwGetPrimaryMonitor(), NULL);
    glfwMakeContextCurrent(window);

    checkGLErrors("Window created: ");

    // Set the required callback functions
    //glfwSetWindowUserPointer(GLFWwindow, window);
    glfwSetKeyCallback(window, key_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    checkGLErrors("Callback functions set: ");

    // Options
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    checkGLErrors("Input mode set: ");

    // Initialize GLEW to setup the OpenGL Function pointers
    glewExperimental = GL_TRUE;

    checkGLErrors("GLEW is experimental: ");

    err = glewInit();

    std::cout << "Using GLEW " << glewGetString(GLEW_VERSION) << std::endl;

    if(GLEW_OK != err)
    {
        std::cerr << "GLEW error: " << glewGetErrorString(err) << std::endl;
    }

    //checkGLErrors("Glew initialized: ");
    glGetError();
    //this version of GLEW casues invalid enum error which is no problem

    // Define the viewport dimensions
    glViewport(0, 0, mode->width, mode->height);

    checkGLErrors("Viewport defined: ");

    // Setup some OpenGL options
    glEnable(GL_DEPTH_TEST);

    checkGLErrors("GL initialized: ");

    return window;
}

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
    //cout << key << endl;
    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    if (key >= 0 && key < 1024)
    {
        if(action == GLFW_PRESS)
            keys[key] = true;
        else if(action == GLFW_RELEASE)
            keys[key] = false;
    }
}

void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
    if(firstMouse)
    {
        lastx = xpos;
        lasty = ypos;
        firstMouse = false;
    }

    GLfloat xoffset = xpos - lastx;
    GLfloat yoffset = lasty - ypos;  // Reversed since y-coordinates go from bottom to left

    lastx = xpos;
    lasty = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}


void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(yoffset);
}



void doMovement()
{
    // Camera controls
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
}

void fillObjects(std::vector<House> &houses, std::vector<Street> &streets)
{
    std::vector<glm::vec3> housePositions;
    housePositions.resize(6);


    //Offsets for window positions
    GLfloat x = -1, z = -17;

    for(int i = 0; i < 6; i ++)
    {
                                                   //left side of street
        if(i % 2)
        {
            housePositions[i] = glm::vec3(x + 6, 0.001, z);
            z += 6;
        }
        else
        {
            housePositions[i] = glm::vec3(x, 0.001, z);
        }

    }

    Texture texture("../resources/container.jpg", "../resources/window.jpg", "../resources/street.jpg");

    streets.emplace_back(glm::vec3(0, 0.001, 0), texture.getTextureStreet());

    houses.reserve(16);

    houses.emplace_back(housePositions[0], texture.getTextureHouse(), texture, false);
    for(auto &i: housePositions)
    {
        houses.emplace_back(i, texture.getTextureHouse(), texture, false);
    }

    glm::vec3 extrapos = glm::vec3(1, 0.001, 4);
    houses.emplace_back(extrapos, texture.getTextureHouse(), texture, true);
    extrapos = glm::vec3(7, 0.001, 4);
    houses.emplace_back(extrapos, texture.getTextureHouse(), texture, true);
    extrapos = glm::vec3(9, 0.001, -5);
    houses.emplace_back(extrapos, texture.getTextureHouse(), texture, false);
}
