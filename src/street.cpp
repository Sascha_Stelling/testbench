#include "street.h"
static std::vector<GLfloat> _STREET_VERTICES = {
        1.5f,  0.0f, -20.0f, 0.0f,  0.0f,             //bottom left
        1.5f,  0.0f, 3.5f,  0.0f,  38.0f,            //top left
        4.5f,  0.0f, 3.5f,  3.0f,  38.0f,            //top right

        1.5f,  0.0f, -20.0f, 0.0f,  0.0f,             //bottom left
        4.5f,  0.0f, -20.0f, 3.0f,  0.0f,             //bottom right
        4.5f,  0.0f, 3.5f,  3.0f,  38.0f,            //top right

        4.5f,  0.0f, 3.5f, 0.0f,  0.0f,             //bottom left
        4.5f,  0.0f,  0.5f, 0.0f,  4.85f,             //top left
        40.0f, 0.0f,  0.5f, 40.0f, 4.85f,            //top right

        4.5f,  0.0f, 3.5f, 0.0f,  0.0f,             //bottom left
        40.0f, 0.0f, 3.5f, 40.0f, 0.0f,             //bottom right
        40.0f, 0.0f,  0.5f, 40.0f, 4.85f,             //top right

        12.0f,  0.0f, 0.5f, 0.0f,  0.0f,             //bottom left
        12.0f,  0.0f, -10.0f,  0.0f,  16.0f,            //top left
        15.0f,  0.0f, -10.0f,  3.5f,  16.0f,            //top right

        12.0f,  0.0f, 0.5f, 0.0f,  0.0f,             //bottom left
        15.0f,  0.0f, 0.5f, 3.5f,  0.0f,             //bottom right
        15.0f,  0.0f, -10.0f,  3.5f,  16.0f,            //top right

    };

Street::Street(glm::vec3 position, GLuint texID)
    : BaseObject(VAO, VBO, texID, scale, position, window)
{
    scale = glm::vec3(1, 1, 1);
    NUM_VERT = 35;
    NUM_VERT_COMPONENTS = 3;
    NUM_TEX_COMPONENTS = 2;
    setGeometry(_STREET_VERTICES);
    init_vaos();
}

Street::Street(Street &other)
    : BaseObject(other.VAO, other.VBO, other.texID, other.scale, other.position, other.window)
{
    setGeometry(_STREET_VERTICES);
    init_vaos();
}

Street::Street(Street &&other)
{
    camera = other.camera;
    position = other.position;
    texID = other.texID;
    VAO = other.VAO;
    VBO = other.VBO;
    window = other.window;
    init_vaos();
}
