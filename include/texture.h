#pragma once

#include <string>
#include <iostream>
#pragma once

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Other Libs
#include <SOIL/SOIL.h>

class Texture
{
public:
    Texture();
    Texture(std::string houseTexPath, std::string windowTexPath, std::string streetTexPath);
    void init_textures(std::string houseTexPath, std::string windowTexPath, std::__cxx11::string streetTexPath);
    GLuint getTextureWindow();
    GLuint getTextureHouse();
    GLuint getTextureStreet();
private:
    GLuint texture_house;
    GLuint texture_window;
    GLuint texture_street;
};
