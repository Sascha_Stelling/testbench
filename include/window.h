#pragma once

#include "baseobject.h"

class Window: public BaseObject
{
public:
    Window(glm::vec3 position, glm::vec3 scale, GLuint texID);
    Window(Window &other);
    Window(Window&& other);
    std::vector<glm::vec3> getCorners();

private:

};
