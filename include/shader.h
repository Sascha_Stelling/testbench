#ifndef SHADER_H
#define SHADER_H

#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>
#include <iterator>
#include <sstream>
#include <ostream>
#include <stdio.h>


#include <GL/glew.h>

class Shader
{
public:
    GLuint Program;
    GLboolean test = false;
    // Constructor generates the shader on the fly
    Shader(Shader&& other)
        : Program(other.Program)
    {
    }

    Shader(const Shader& other)
        : Program(other.Program)
    {
    }

    Shader& operator =(const Shader& other)
    {
        Program = other.Program;
        return *this;
    }

    void testShader()
    {
        if(test)
        {
            std::cout << "Shader working!" << std::endl;
        }
        else
        {
            std::cerr << "Shader not compiled!" << std::endl;
        }
    }

    Shader(const GLchar* vertexPath, const GLchar* fragmentPath)
    {
        // 1. Retrieve the vertex/fragment source code from filePath
        std::string vertexCode;
        std::string fragmentCode;
        std::ifstream vShaderFile;
        std::ifstream fShaderFile;
        // ensures ifstream objects can throw exceptions:
        vShaderFile.exceptions (std::ifstream::badbit);
        fShaderFile.exceptions (std::ifstream::badbit);
        try
        {
            fShaderFile = std::ifstream(fragmentPath);
            vShaderFile = std::ifstream(vertexPath);
            vertexCode = std::string((std::istreambuf_iterator<char>(vShaderFile)),
                        std::istreambuf_iterator<char>());
            fragmentCode = std::string((std::istreambuf_iterator<char>(fShaderFile)),
                        std::istreambuf_iterator<char>());
        }
        catch (std::ifstream::failure e)
        {
            std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
        }


        const GLchar* vShaderCode = vertexCode.c_str();
        const GLchar * fShaderCode = fragmentCode.c_str();
        // 2. Compile shaders
        GLuint vertex, fragment;
        GLint success;
        GLchar infoLog[512];
        // Vertex Shader
        vertex = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertex, 1, &vShaderCode, NULL);
        glCompileShader(vertex);
        // Print compile errors if any
        glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(vertex, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
        }
        // Fragment Shader
        fragment = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragment, 1, &fShaderCode, NULL);
        glCompileShader(fragment);
        // Print compile errors if any
        glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
        if (!success)
        {
            glGetShaderInfoLog(fragment, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
        }
        // Shader Program
        this->Program = glCreateProgram();
        glAttachShader(this->Program, vertex);
        glAttachShader(this->Program, fragment);
        glLinkProgram(this->Program);
        // Print linking errors if any
        glGetProgramiv(this->Program, GL_LINK_STATUS, &success);
        if (!success)
        {
            glGetProgramInfoLog(this->Program, 512, NULL, infoLog);
            std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
        }
        // Delete the shaders as they're linked into our program now and no longer necessery
        glDeleteShader(vertex);
        glDeleteShader(fragment);
        test = true;

    }
    // Uses the current shader
    void Use()
    {
        glUseProgram(this->Program);
    }
};

#endif

