#pragma once

#include "baseobject.h"

class Street: public BaseObject
{
public:
    Street(glm::vec3 position, GLuint texID);
    Street(Street &other);
    Street(Street &&other);

private:

};
