#pragma once

// GLEW
#define GLEW_STATIC
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

// GL includes
#include "shader.h"
#include "camera.h"

// GLM Mathematics
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Other Libs
#include <../libs/SOIL.h>

class BaseObject
{
public:
    BaseObject();
    ~BaseObject();
    BaseObject(GLuint VAO, GLuint VBO, GLuint texture, glm::vec3& scale, glm::vec3& position, GLFWwindow* window);
    BaseObject(BaseObject&& other);
    BaseObject(const BaseObject& other);
    void init_vaos();
    void draw(GLFWwindow *window, Camera &camera, Shader &shader);
    void setTexture(std::string texPath);
    void setGeometry(std::vector<GLfloat> &vertices);
    void setScale(glm::vec3 newscale);
    glm::vec3 getPosition();
    std::vector<glm::vec3> getCorners();
    void drawPosition();

protected:
    static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
    static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
    static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
    GLuint VAO, VBO, texID;
    GLshort NUM_VERT, NUM_VERT_COMPONENTS, NUM_TEX_COMPONENTS;
    GLFWwindow* window;
    std::vector<GLfloat> vertices;
    std::string texPath;
    glm::vec3 position;
    glm::vec3 scale;
    Camera camera;
};

void doMovement();

void checkGLErrors(std::__cxx11::string errorstring);

glm::mat4 myscale(glm::mat4 model, glm::vec3 scale, glm::vec3 position);
glm::mat4x4 transform(glm::mat4 &model, glm::vec3 position, glm::vec3 scale);
void printvec3(glm::vec3 vector);

