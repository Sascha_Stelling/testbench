#pragma once

#include "baseobject.h"
#include "window.h"
#include "texture.h"


class House :public BaseObject
{
public:
    House(glm::vec3& position, GLuint texID, Texture& texobject, bool horizontal);
    House(House&& other);
    House(House &other);
    void fillwindows_h();
    void fillwindows_v();
    void setTexobj(Texture& texobject);
    void draw(GLFWwindow *window, Camera &camera, Shader &shader);
    std::vector<glm::vec3> getWindowCorners();
    void drawWindowPosition();
    int getNumWindows();
    void setOrientation(bool horizontal);

private:
    std::string vshaderPath, fshaderPath;
    std::vector<Window> windows;
    std::vector<glm::vec3> windowPositions;
    Texture* texobj;
    GLshort WIDTH = 4, HEIGHT = 2, DEPTH = 2;
    bool horizontal = false;
};
